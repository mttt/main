# CHANGELOG.md

## 0.99.4 (2020-06-08)

- Refactored task manager featuring execution queues

## 0.99.3

- Added parameters on TER evaluations
- Added TER evaluation files previewing and downloading
- Added source/target line count info on models
- Improved forms 

## 0.99.2

- Added translation preview, showing both source and target, line by line.
- Added tokenizer and truecaser output to _test_.

## 0.99.1

- Added preview on compressed files.
- Model list splitted in _owned_ and _shared_.
- Removed 'change language' option from model properties.
- Added _deleted_ and _error_ model states.

## v0.99

- Added file translation.
- Added password recovery.
- Test input are now tokenized and truecased before translation.
- In model creation, when language model file is missing, target file is used.
- Added basic form validation to model creation form.
- [BUGFIX] Models referenced by a translation can now be deleted.
- [BUGFIX] Translations whose input have not trailing new line missed the last sentence.