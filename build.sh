usage()
{
    echo "Usage: build [ -c | --cpu ]
  Options:
    -c, --cpu	Build OpenNMT images using the PyTorch cpu version"
    exit 2
}

# $1 image name
# $2 context
# $3 dockerfile [optional]
function build {
    docker tag $1 $1:old
    docker build -t $1 ${3:+-f $3} $2
    docker rmi $1:old
}

CPU=0

PARSED_ARGUMENTS=$(getopt -a -n build -o c --long cpu -- "$@")
echo "PARSED_ARGUMENTS is $PARSED_ARGUMENTS"
eval set -- "$PARSED_ARGUMENTS"
while :
do
  case "$1" in
    -c | --cpu) CPU=1  ; shift ;;
    --) shift; break ;;
  esac
done

if [ $CPU == 1 ]
then
  ONMT_BASE="base-cpu"
else
  ONMT_BASE="base-gpu"
fi

build mttt-web-server server

build task-manager	task-manager	task-manager/dockerfiles/Dockerfile

MTTT_REGISTRY=registry.gitlab.com
MTTT_MOSES_IMG=registry.gitlab.com/mttt/moses/base
MTTT_REG_USERNAME=gitlab+deploy-token-182658
MTTT_REG_TOKEN=DFgMb86nmGWomxDUh_xY
docker login $MTTT_REGISTRY -u $MTTT_REG_USERNAME -p $MTTT_REG_TOKEN
docker pull $MTTT_MOSES_IMG
docker tag moses-base moses-base:old
docker tag $MTTT_MOSES_IMG moses-base
docker rmi moses-base:old
docker logout $MTTT_REGISTRY
build moses-train       moses   moses/dockerfiles/train.dockerfile
build moses-translate   moses   moses/dockerfiles/translate.dockerfile

build ev.bleu.img   evaluation  evaluation/bleu.dockerfile
build ev.wer.img    evaluation  evaluation/wer.dockerfile
build ev.ter.img    evaluation  evaluation/ter.dockerfile

build onmt-base		      onmt	onmt/dockerfiles/$ONMT_BASE.dockerfile
build onmt-preprocess 	onmt	onmt/dockerfiles/preprocess.dockerfile
build onmt-train 	      onmt	onmt/dockerfiles/train.dockerfile
build onmt-translate 	  onmt	onmt/dockerfiles/translate.dockerfile

build preprocess-base 		  preprocess 	preprocess/dockerfiles/base.dockerfile
build preprocess-tokenizer  preprocess 	preprocess/dockerfiles/tokenizer.dockerfile
build preprocess-truecaser  preprocess 	preprocess/dockerfiles/truecaser.dockerfile
build preprocess-cleaner  	preprocess 	preprocess/dockerfiles/cleaner.dockerfile
build preprocess-splitter	  preprocess 	preprocess/dockerfiles/splitter.dockerfile

