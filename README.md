# MTTT

## Installation

### Install docker

First, install [docker](https://docker.com) and [docker-compose](https://docker.com).

### Configure network settings

Then, create a dummy network interface with the static ip ```172.40.0.1```.

For example, in ```netctl```:

```
netctl edit mttt0
```
```
Interface=mttt0
Connection=dummy
IP=static
Address=('172.40.0.1/31')
```

Then

```
netctl enable mttt0

netctl start mttt0
```

Also, it is posible to use the ```ip``` command instead:

```
ip link add name mttt0 type dummy
ip address add 172.40.0.1/28 dev mttt0
```

But this method has the inconvinient of not beign permanet between reboots, so if you restart your system it is most likely that the docker daemon will crash at startup.

Add the following line to ```/etc/hosts```:

```
172.40.0.1  docker-daemon
```

### Configure docker daemon

Configure the docker daemon in order to enable a TCP port for accessing the API.

If you use systemctl as interface, you can run the following:

```
systemctl edit docker
```
```
[Service]
ExecStart=
ExecStart=/usr/bin/dockerd -H unix:///var/run/docker.sock -H tcp://docker-daemon:4243
```

For other methods, more information is available [here](https://docs.docker.com/config/daemon/#configure-the-docker-daemon).

### Clone the repo

Clone this repo using the `--recurse-submodules` flag, for example:

```
git clone --recurse-submodules https://gitlab.com/mttt/main mt3
```

In this case, the repository will be cloned into `mt3` folder.

### Build the images

Now, inside the cloned repository folder build the images by running:

```
./build.sh
```

If the host machine has no CUDA compatible GPU, you can use the `-c` or `--cpu` flag for a cpu-only build:

```
./build.sh -c
```

## Run

To launch the system, run:

```
docker-compose up -d
```

## Reset

If you want to get the application back to a clean state, you can use docker-compose functions to reset the application, by using a `down` followed by an `up`:

```
docker-compose down --volumes
docker-compose up -d
```

This will cause all mt3 containers, volumes and network to be removed and then created again.

**WARNING:** This action cannot be undone, so all your application data including users, models and translations will be lost.